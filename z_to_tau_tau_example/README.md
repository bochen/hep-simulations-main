# Creating and analyzing dijet sample

## Running madgraph
Open Madgraph with
```bash
../MG5_aMC_v2_9_5/bin/mg5_aMC 
```
Now run
```
generate p p > z
output z_generation_process
launch
```

We'll use the default options, so for the next two prompts just enter
```
0
0
```
and Madgraph should start chugging along, creating 10,000 z events. Once it's done, exit Madgraph.

## Running Delphes
Madgraph made for us an LHE file with these events. First, unzip it.
```bash
gunzip z_generation_process/Events/run_01/unweighted_events.lhe.gz
```

Now, let's run Delphes. If you don't want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```
If you do want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS_PileUp.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```


This takes as input
1. The card describing our detector (here I use ATLAS)
2. The card telling Pythia what to do (here I use a very basic card)
3. The name of our output file

This will produce 1000 events in *z_to_tau_tau_output.root*

## Analysis
Run *analysis.C*
This finds events with 2 tau-tagged jets and plots their invariant mass, which should be the mass of the Z boson.
![tau tau mass](m_tautau.png)