# Running simulations with Madgraph, Delphes, and Pythia

## Getting Started
- Run *setupSimulations.sh*
- This will install Madgraph, Delphes, and Pythia here and make them all talk to each other
- Do this **every time** you want to run simulations to initialize the software. It will only download them all the first time, but it's needed every time to initialize them.

## Simple Example
Minimal running example with analysis script can be found in [dijet-example](dijet-example)

##To add the LHAPDF
set lhapdf /cvmfs/sft.cern.ch/lcg/releases/LCG_87/MCGenerators/lhapdf/6.1.6/x86_64-slc6-gcc49-opt/

##To setup for the delphes 
source /cvmfs/sft.cern.ch/lcg/views/LCG_99/x86_64-centos7-gcc10-opt/setup.sh
